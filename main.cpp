#include <cstring>
#include "starter_code/common.h"
#include <iostream>
#include "BaseApplication.h"
#include "Client.h"
#include "Server.h"
#include "SocketImplementation.h"
#include "ace/Get_Opt.h"

int parse_arguments(int argc, ACE_TCHAR * argv[], ACE_TCHAR *mutexOut){
    int n = -1;
    int opt = 0;
    ACE_Get_Opt get_opt (argc, argv, ACE_TEXT ("p:m"));
    while ((opt = get_opt ()) != EOF) {
        switch (opt) {
            case 'p':
                n = ACE_OS::atoi(get_opt.opt_arg());
                break;
            case 'm':
                *mutexOut = 1;
                break;
            default:
                ACE_OS::exit(EXIT_FAILURE);
                break;
        }
    }
    return n;
}

int parent_job(local_id id, local_id n, int pipes_log_fd, int events_log_fd)
{
    ServerApplication parent(PARENT_ID, n, pipes_log_fd, events_log_fd, std::make_unique<SocketImplementation>());
    //создаем acceptor
    SocketImplementation::Acceptor acceptor(static_cast<SocketImplementation &>(parent.get_transport()));
    const ACE_INET_Addr addr(SocketImplementation::get_acceptor_port(PARENT_ID));
    //открываем сокет
    if(acceptor.open(addr) == -1){
        ACE_OS::exit(EXIT_FAILURE);
    }
    while (!parent.is_done()) {
        ACE_Reactor::instance()->handle_events();
    }
    return 0;
}

void child_job(local_id id, local_id n, bool mutexl,
               int pipes_log_fd, int events_log_fd)
{
    Client child(id, n, mutexl, pipes_log_fd, events_log_fd, std::make_unique<SocketImplementation>());
    //создаем accceptor
    SocketImplementation::Acceptor acceptor(static_cast<SocketImplementation &>(child.get_transport()));
    //открываем сокет (пассивный)
    const ACE_INET_Addr addr(SocketImplementation::get_acceptor_port(child.get_id()));
    if (acceptor.open(addr) != 0) {
        ACE_OS::exit(EXIT_FAILURE);
    }
    //создаем connector
    SocketImplementation::Connector connector(static_cast<SocketImplementation &>(child.get_transport()));
    // создаем активное соединение для отправки данных
    for (local_id i = 0; i < child.get_id(); ++i) {
        const ACE_INET_Addr addr_acc(SocketImplementation::get_acceptor_port(i));
        const ACE_INET_Addr addr_conn(SocketImplementation::get_connector_port(child.get_id(), i));
        SocketImplementation::Svc_Handler * peer = nullptr;
        int ret_connect = connector.connect(peer, addr_acc, ACE_Synch_Options::defaults, addr_conn, 1);
        if (ret_connect == -1) {
            ACE_OS::exit(EXIT_FAILURE);
        }
    }
    while (!child.is_done()) {
        ACE_Reactor::instance()->handle_events();
    }
}

int ACE_TMAIN(int argc, ACE_TCHAR * argv[])
{
    int n;
    ACE_TCHAR mutexl = 0;
    n = parse_arguments(argc, argv, &mutexl);
    ++n;

    int pipes_log_fd = open(pipes_log, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
    int events_log_fd = open(events_log, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);

    for (local_id i = 1; i < n; ++i) {
        int cpid = fork();
        //находиммся в процессе-потомке, так как fork возвращает 0, есл мы в процессе-потомке
        if (cpid == 0) {
            child_job(i, n, mutexl, pipes_log_fd, events_log_fd);
            ACE_OS::exit(EXIT_SUCCESS);
        }
    }

    local_id ret_parent = parent_job(PARENT_ID, n, pipes_log_fd, events_log_fd);
    return ret_parent;
}