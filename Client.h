#pragma once

#include "BaseApplication.h"

class Client : public BaseApplication
{
    using super = BaseApplication;
public:
    Client(local_id id, local_id n, bool mutexl,
            int pipes_log_fd, int events_log_fd, std::unique_ptr<ISOCKTransport> &&);

private:
    void cs_work();
    int request_cs();
    void cs_access_granted();
    void print(const ACE_TCHAR * s) const;
    int release_cs();

    void done();

private:
    void handle_connected() final;
    void handle_all_started_received() final;

    void cs_reply_handler(local_id from, const Message & msg) final;
    void cs_release_handler(local_id from, const Message & msg) final;

private:
    const bool m_mutexl = true;
    local_id m_reply_counter = 0;
    int m_cs_work = 0;
};
