#include <fcntl.h>
#include <cstring>
#include <pa2345.h>
#include <array>
#include <iostream>
#include <ace/ace_wchar.h>
#include "BaseApplication.h"
#include "starter_code/ipc.h"

BaseApplication::BaseApplication(const local_id id, const local_id n,
                       const int pipes_log_fd, const int events_log_fd, std::unique_ptr<ISOCKTransport> && transport)
        : m_id(id)
        , m_n(n)
        , m_pid(getpid())
        , m_ppid(getppid())
        , m_pipes_log_fd(pipes_log_fd)
        , m_events_log_fd(events_log_fd)
        , m_transport(std::move(transport))
{
    m_transport->register_app(*this);
}

BaseApplication::~BaseApplication()
{
    close(m_pipes_log_fd);
    close(m_events_log_fd);
//    exit(EXIT_SUCCESS);
}

void BaseApplication::tee_log(const ACE_TCHAR * str, const int fd)
{
//    const auto len = std::strlen(str);
//    write(fd, str, len);
//    write(STDOUT_FILENO, str, len);
}

void BaseApplication::handle_message(const Message & msg, const local_id from)
{

    if (msg.s_header.s_local_time > m_ts) {
        m_ts = msg.s_header.s_local_time;
    }
    ++m_ts;

//    std::array<ACE_TCHAR, 128> buf{};

    switch (msg.s_header.s_type) {
        case STARTED:
            started_handler(from, msg);
            break;
        case STOP:
            stop_handler(from, msg);
            break;
        case DONE:
            done_handler(from, msg);
            break;
        case CS_REQUEST:
            cs_request_handler(from, msg);
            break;
        case CS_REPLY:
            cs_reply_handler(from, msg);
            break;
        case CS_RELEASE:
            cs_release_handler(from, msg);
            break;
        default:
            exit(EXIT_FAILURE);
            break;
    }
}

void BaseApplication::started_handler(const local_id, const Message & msg)
{
    if (++m_alive_num == m_n) {
        handle_all_started_received();
    }
}

void BaseApplication::stop_handler(local_id from, const Message &msg) {
//    ACE_OS::exit(EXIT_FAILURE);
}

void BaseApplication::cs_request_handler(const local_id from, const Message & msg)
{
    m_queue.push({from, msg.s_header.s_local_time});
    Message reply;
    reply.s_header.s_magic = MESSAGE_MAGIC;
    reply.s_header.s_type = CS_REPLY;
    reply.s_header.s_payload_len = 0;
    send(from, reply);
}

void BaseApplication::cs_release_handler(const local_id, const Message &)
{
    m_queue.pop();
}

void BaseApplication::cs_reply_handler(local_id from, const Message &msg) {
    ACE_OS::exit(EXIT_FAILURE);
}

void BaseApplication::done_handler(const local_id, const Message & msg)
{
    if (--m_alive_num == 1) { // only parent
        handle_all_done_received();
    }
}

bool BaseApplication::send(local_id dst, const Message & msg)
{
    if (dst < 0 || dst >= m_n || dst == m_id) {
        return false;
    }

    switch (msg.s_header.s_type) {
        case ACK:
        case STOP:
        case CS_REQUEST:
        case CS_REPLY:
        case CS_RELEASE:
            if (msg.s_header.s_payload_len != 0) {
                return false;
            }
            break;
        case STARTED:
        case DONE:
        case TRANSFER:
        case BALANCE_HISTORY:
            if (msg.s_header.s_payload_len < 0) {
                return false;
            }
            break;
        default:
            return false;
    }

    const_cast<Message &>(msg).s_header.s_local_time = ++(m_ts);

    return m_transport->send(dst, msg);
}

bool BaseApplication::send_multicast(const Message & msg)
{
    for (local_id i = 0; i < m_n; ++i) {
        if (i == m_id) {
            continue;
        }
        const bool ok = send(i, msg);
        if (!ok) {
            return false;
        }
        --m_ts;
    }
    ++m_ts;
    return true;
}

void BaseApplication::handle_all_started_received()
{
    std::array<ACE_TCHAR, 128> buf{};
    snprintf(buf.begin(), buf.max_size(), log_received_all_started_fmt, m_ts,  m_id);
    tee_log(buf.begin(), m_events_log_fd);
}

void BaseApplication::handle_all_done_received()
{
    m_done = true;
    std::array<ACE_TCHAR, 128> buf{};
    snprintf(buf.begin(), buf.max_size(), log_received_all_done_fmt, m_ts, m_id);
    tee_log(buf.begin(), m_events_log_fd);
}
