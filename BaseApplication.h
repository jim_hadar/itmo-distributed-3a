#pragma once

#include <queue>

#include <unistd.h>
#include <fcntl.h>
#include <cstdlib>

#include "starter_code/ipc.h"

#include "ace/ace_wchar.h"
#include "ace/Svc_Handler.h"
#include "ace/SOCK_Stream.h"
#include "ace/Acceptor.h"

class BaseApplication
{
public:
    class ISOCKTransport;

    BaseApplication(local_id id, local_id n, int pipes_log_fd, int events_log_fd, std::unique_ptr<ISOCKTransport> &&);
    virtual ~BaseApplication();

    local_id get_n() const
    { return m_n; }

    local_id get_id() const
    { return m_id; }

    ISOCKTransport & get_transport()
    { return *m_transport; }

    bool is_done() const
    { return m_done; }

public:
    // Transport callbacks
    virtual void handle_connected() = 0;
    void handle_message(const Message & msg, local_id from);

protected:
    bool send(local_id dst, const Message & msg);
    bool send_multicast(const Message & msg);

protected:
    struct QueueElement
    {
        local_id id;
        timestamp_t ts;

        bool operator < (const QueueElement & rhs) const
        { return ts != rhs.ts ? ts > rhs.ts : id > rhs.id; }
    };

    void tee_log(const ACE_TCHAR * str, int fd);

    virtual void started_handler(local_id from, const Message & msg);
    virtual void cs_request_handler(local_id from, const Message & msg);
    virtual void cs_reply_handler(local_id from, const Message & msg) = 0;
    virtual void cs_release_handler(local_id from, const Message & msg);
    virtual void done_handler(local_id from, const Message & msg);
    virtual void stop_handler(local_id from, const Message & msg);

    virtual void handle_all_started_received();
    virtual void handle_all_done_received();

protected:
    const local_id m_id;
    const local_id m_n; // parent + children
    const int m_pid;
    const int m_ppid;
    const int m_pipes_log_fd;
    const int m_events_log_fd;
    timestamp_t m_ts = 0;
    local_id m_alive_num = 1; // only for parent
    std::priority_queue<QueueElement> m_queue;
    std::unique_ptr<ISOCKTransport> m_transport;
    bool m_done = false;
};

class BaseApplication::ISOCKTransport
{
public:
    virtual bool send(local_id dst, const Message & msg) = 0;
    virtual void register_app(BaseApplication & app) = 0;
};