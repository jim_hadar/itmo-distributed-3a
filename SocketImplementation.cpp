#include <iostream>
#include <tgmath.h>
#include "SocketImplementation.h"
#include "BaseApplication.h"

void SocketImplementation::register_app(BaseApplication & app)
{
    m_pending = app.get_n() - 1;
    m_svc_handlers.resize(app.get_n());
    m_app = &app;
}

bool SocketImplementation::send(local_id dst, const Message & msg)
{
    const auto n = m_svc_handlers[dst]->peer().send(&msg, sizeof(MessageHeader) + msg.s_header.s_payload_len);
    return n == static_cast<ssize_t>(sizeof(MessageHeader) + msg.s_header.s_payload_len);
}

//connect handle
int SocketImplementation::open(Svc_Handler & handler)
{
    ACE_INET_Addr addr;
    handler.peer().get_remote_addr(addr);
    const auto from = get_id(addr.get_port_number(), !handler.is_connector());
    m_svc_handlers[from] = &handler;
    if (--m_pending == 0) {
        // отправляем multicast сообщение о том, что сервис стартовал
        //если это "ребенок"
        m_app->handle_connected();
    }
    return 0;
}

int SocketImplementation::close(Svc_Handler & handler)
{
    return 0;
}

int SocketImplementation::handle_timeout(Svc_Handler & handler)
{
    return 0;
}

//input message
int SocketImplementation::handle_input(const Svc_Handler & handler)
{
    Message msg;
    //ждем, пока получим какое-либо сообщение
    auto n = handler.peer().recv(&msg.s_header, sizeof(MessageHeader));
    if (n != sizeof(MessageHeader)) {
        return -1;
    }
    //ждем, пока получим тело сообщения
    if (msg.s_header.s_payload_len > 0) {
        handler.peer().recv(&msg.s_payload, msg.s_header.s_payload_len);
    }
    ACE_INET_Addr addr;
    //получаем адрес отправителя
    handler.peer().get_remote_addr(addr);
    //получаем id процесса (локальный номер для нас)
    const auto from = get_id(addr.get_port_number(), !handler.is_connector());
    //вызываем обработчик, обрабатывабщий все возможные типы сообщений
    m_app->handle_message(msg, from);
    return 0;
}

local_id SocketImplementation::get_id(const u_short port, const bool is_connector)
{
    return is_connector ? get_connector_id(port, m_app->get_id()) : get_acceptor_id(port);
}

u_short SocketImplementation::get_connector_port(const local_id from, const local_id to)
{
    const auto ports_before_from = from * (from + 1) / 2;
    return PARENT_PORT - 1 + ports_before_from + (from - to);
}

local_id SocketImplementation::get_connector_id(const u_short port, const local_id to)
{
    return static_cast<local_id>((sqrt(9 + 8 * (port - PARENT_PORT + to + 1)) - 3) / 2);
}

u_short SocketImplementation::get_acceptor_port(const local_id id)
{
    const auto ports_before = id * (id + 1) / 2;
    return PARENT_PORT + ports_before + id;
}

local_id SocketImplementation::get_acceptor_id(const u_short port)
{
    return static_cast<local_id>((sqrt(9 + 8 * (port - PARENT_PORT)) - 3) / 2);
}
