#include "Server.h"

#include "sys/wait.h"

void ServerApplication::handle_connected()
{
}

void ServerApplication::handle_all_done_received()
{
    //ждем все дочерние процессы
    for (local_id i = 1; i < m_n; ++i)
    {
        wait(nullptr);
    }
    super::handle_all_done_received();
}

void ServerApplication::cs_reply_handler(local_id from, const Message & msg)
{
    exit(EXIT_FAILURE);
}