#pragma once

#include "BaseApplication.h"

class ServerApplication : public BaseApplication
{
    using super = BaseApplication;
public:
    using super::super;

private:
    void handle_connected() final;

private:
    void cs_reply_handler(local_id from, const Message & msg) final;

    void handle_all_done_received() final;
};
