#include <pa2345.h>
#include <cstdio>
#include <array>
#include <ace/ace_wchar.h>
#include <iostream>
#include "Client.h"

Client::Client(const local_id id, const local_id n, const bool mutexl,
                  const int pipes_log_fd, const int events_log_fd, std::unique_ptr<ISOCKTransport> && transport)
        : super(id, n, pipes_log_fd, events_log_fd, std::move(transport))
        , m_mutexl(mutexl)
{
    ++m_alive_num; // self
}

void Client::handle_connected()
{
    Message msg;
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_type = STARTED;
    msg.s_header.s_payload_len = snprintf(msg.s_payload, MAX_PAYLOAD_LEN, log_started_fmt,
                                          m_ts,
                                          m_id,
                                          m_pid,
                                          getppid(),
                                          0);
    tee_log(msg.s_payload, m_events_log_fd);
    send_multicast(msg);
}

void Client::handle_all_started_received()
{
    super::handle_all_started_received();
    cs_work();
}

void Client::cs_work()
{
    if (m_mutexl) {
        request_cs();
    }
    else {
        const auto n = m_id * 5;
        for (int i = 1; i <= n; i++) {
            std::array<ACE_TCHAR, 128> buf{};
            snprintf(buf.begin(), buf.max_size(), log_loop_operation_fmt, m_id, i, n);
            print(buf.begin());
        }
        done();
    }
}

int Client::request_cs()
{
    Message msg;
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_type = CS_REQUEST;
    msg.s_header.s_payload_len = 0;
    m_reply_counter = m_n - 1;
    send_multicast(msg);
    m_queue.push({m_id, m_ts});
    return 0;
}

int Client::release_cs()
{
    Message msg;
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_type = CS_RELEASE;
    msg.s_header.s_payload_len = 0;
    send_multicast(msg);
    m_queue.pop();
    return 0;
}

void Client::cs_access_granted()
{
    const int n = m_id * 5;
    std::array<ACE_TCHAR, 128> buf{};
    snprintf(buf.begin(), buf.max_size(), log_loop_operation_fmt, m_id, static_cast<int>(++m_cs_work), n);
    print(buf.begin());
    release_cs();
    if (m_cs_work < n) {
        request_cs();
    }
    else {
        done();
    }
}

void Client::print(const ACE_TCHAR * s) const
{
    std::cout << s << std::endl;
}

void Client::done()
{
    Message msg;
    msg.s_header.s_magic = MESSAGE_MAGIC;
    msg.s_header.s_type = DONE;
    msg.s_header.s_payload_len = snprintf(msg.s_payload, MAX_PAYLOAD_LEN, log_done_fmt,
                                            m_ts,
                                            m_id,
                                            0);
    tee_log(msg.s_payload, m_events_log_fd);
    send_multicast(msg);
    done_handler(m_id, msg);
}

void Client::cs_reply_handler(const local_id from, const Message & msg)
{
    if (--m_reply_counter == 0 && m_queue.top().id == m_id) {
        cs_access_granted();
    }
}

void Client::cs_release_handler(local_id from, const Message & msg)
{
    super::cs_release_handler(from, msg);
    if (m_reply_counter == 0 && !m_queue.empty() && m_queue.top().id == m_id) {
        cs_access_granted();
    }
}
