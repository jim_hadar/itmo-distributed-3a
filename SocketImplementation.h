#pragma once

#include <ace/Global_Macros.h>
#include <ace/SOCK_Stream.h>
#include <ace/Synch_Traits.h>
#include <ace/Svc_Handler.h>
#include <ace/SOCK_Acceptor.h>
#include <ace/Connector.h>
#include <ace/SOCK_Connector.h>
#include <ace/Acceptor.h>

#include "BaseApplication.h"

#include "ipc.h"


class SocketImplementation : public BaseApplication::ISOCKTransport
{
public:
    class Svc_Handler;
    class Acceptor;
    class Connector;

public:
    SocketImplementation() = default;

    void register_app(BaseApplication & app) final;
    bool send(local_id dst, const Message & msg) final;

public:
    static constexpr u_short PARENT_PORT = 24000;

    static u_short get_acceptor_port(local_id id);
    static u_short get_connector_port(local_id from, local_id to);
    static local_id get_acceptor_id(u_short port);
    static local_id get_connector_id(u_short port, local_id to);

private:
    local_id get_id(u_short port, bool is_connector);

    // callbacks for Svc_Handler
    int open(Svc_Handler & handler);
    int close(Svc_Handler & handler);
    int handle_timeout(Svc_Handler & handler);
    int handle_input(const Svc_Handler & handler);

private:
    BaseApplication * m_app = nullptr;
    std::vector<Svc_Handler *> m_svc_handlers;
    size_t m_pending;
};

class SocketImplementation::Svc_Handler : public ACE_Svc_Handler<ACE_SOCK_Stream, ACE_NULL_SYNCH>
{
    using super = ACE_Svc_Handler<ACE_SOCK_Stream, ACE_NULL_SYNCH>;
public:
    Svc_Handler()
    { }
    Svc_Handler(SocketImplementation & transport, const bool is_connector)
            : m_transport(&transport)
            , m_is_connector(is_connector)
    { }
public:
    //on open register handle in reactor
    int open(void * p) final
    {
        if (this->reactor()->register_handler(this, READ_MASK) != 0) {
            ACE_OS::exit(EXIT_FAILURE);
        }
        m_transport->open(*this);
        return 0;
    }

    int close(u_long flags) final
    {
        m_transport->close(*this);
        return super::close(flags);
    }

    int handle_timeout(const ACE_Time_Value & a1, const void * a2) final
    {
        m_transport->handle_timeout(*this);
        return super::handle_timeout(a1, a2);
    }

    int handle_input(ACE_HANDLE) final
    {
        return m_transport->handle_input(*this);
    }

    bool is_connector() const
    { return m_is_connector; }

private:
    SocketImplementation * m_transport = nullptr;
    const bool m_is_connector = false;
};

class SocketImplementation::Acceptor : public ACE_Acceptor<Svc_Handler, ACE_SOCK_ACCEPTOR>
{
public:
    Acceptor(SocketImplementation & transport)
            : m_transport(transport)
    { }
private:
    int make_svc_handler(Svc_Handler *& sh) final
    {
        if (sh == nullptr) {
            sh = new Svc_Handler(m_transport, false);
        }
        sh->reactor(this->reactor());
        return 0;
    }
private:
    SocketImplementation & m_transport;
};

class SocketImplementation::Connector : public ACE_Connector<Svc_Handler, ACE_SOCK_CONNECTOR>
{
public:
    Connector(SocketImplementation & transport)
            : m_transport(transport)
    { }
private:
    int make_svc_handler(Svc_Handler *& sh) final
    {
        if (sh == nullptr) {
            sh = new Svc_Handler(m_transport, true);
        }
        sh->reactor(this->reactor());
        return 0;
    }
private:
    SocketImplementation & m_transport;
};
